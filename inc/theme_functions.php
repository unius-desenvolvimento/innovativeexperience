<?php

/* Disable WordPress Admin Bar for all users but admins. */

show_admin_bar(false);

add_action( 'after_setup_theme', 'register_theme_menu' );
function register_theme_menu() {
    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus( array(
        'Top'   => __( 'Top primary menu', 'theme_luapp' ),
        'Footer' => __( 'Footer menu', 'theme_luapp' ),
    ) );
}

add_theme_support( 'post-thumbnails' ); 

/*
 * Pagination
 */
function wp_pagination($pages = '', $range = 9) {
    global $wp_query, $wp_rewrite;
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
    $pagination = array(
        'base'       => @add_query_arg('page','%#%'),
        'format'     => '',
        'total'      => $wp_query->max_num_pages,
        'current'    => $current,
        'show_all'   => false,
        'prev_text'  => ('<'),
        'next_text'  => ('>'),
        'type'       => 'plain'
    );
    $currentURL = get_pagenum_link(1,true);
    $explodedLink = explode('/?', $currentURL);
    $singleLink = $explodedLink[0];
    $params = @explode('/', $explodedLink[1]);
    if ($wp_rewrite->using_permalinks()) {
        $pagination['base'] = user_trailingslashit(trailingslashit(remove_query_arg('s', $singleLink)) . 'page/%#%/', 'paged');
    }
    if (!empty($wp_query->query_vars['s'])) $pagination['add_args'] = array('s' => get_query_var('s'));
    echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wp_pagination"><div class="container-pag">'.paginate_links($pagination).'</div></div>';
}
