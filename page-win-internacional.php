<?php
/**
 * The template for displaying Home
 *
 * This is the template that display Home.
 *
 * @package WordPress
 * @subpackage Theme_Luapp
 * @author Descubra
 * @since Theme Luapp 1.0
 */
get_header();?>

<div class="workshop-internacional">
	<div class="container-fluid">
		<div class="banner">
			<div class="img-destaque">
				<?php the_post_thumbnail(); ?>
			</div>
			<div class="dados-destaques">
				<hr>
				<div class="row">
					<div class="dado">
						<?php the_field('win_int_data');?>
					</div>
				</div>
				<div class="row">
					<div class="dado">
						<?php the_field('win_int_local');?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container"> 
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				
				<div class="row">
					<div class="titulo-win-int">
						<h1><?php the_field('titulo_win_int');?></h1>
						<hr>
					</div>
				</div>
				<div class="row">
					<div class="container">
						<div class="conteudo">
							<?php the_content(); ?>
						</div>
					</div>
				</div>				
			<?php endwhile; endif; ?>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
		<div class="chamada">
			<?php the_field('chamada_win_int');?>
		</div>
		<div class="botoes-int">
			<div class="botoes">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="botao-participar">
								<!-- <a href="<?php bloginfo('url'); ?>/quero-visitar"> -->
									<span class="breve">EM BREVE</span>
									<span class="span-botao">Quero participar</span>
								<!-- </a> -->
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="botao-patrocinar"><a href="<?php bloginfo('url'); ?>/quero-patrocinar">

								<span >Quero patrocinar</span>
								</a>
							</div>
						</div>
					</div>
				</div>
		</div>
	</div>

<?php get_footer(); ?>