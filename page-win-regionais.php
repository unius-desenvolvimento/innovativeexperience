<?php
/**
 * The template for displaying Home
 *
 * This is the template that display Home.
 *
 * @package WordPress
 * @subpackage Theme_Luapp
 * @author Descubra
 * @since Theme Luapp 1.0
 */
get_header();?>
<div class="workshop-regionais">
	<div class="container-fluid">
		<div class="banner">
			<div class="img-destaque">
				<?php the_post_thumbnail(); ?>
			</div>
		</div>
	</div>
	<div class="container"> 
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
			<?php 
				$counterElments = 0;
				$counter = 0;
			?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div class="row">
					<div class="titulo-win-reg">
						<h1><?php the_field('titulo_win_reg');?></h1>
						<hr>
					</div>
				</div>
				<div class="row">
					<div class="container">
						<div class="conteudo">
							<?php the_content(); ?>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="eventos">
			<div class="container">
				<div class="row">
						<ul class="navegacao-eventos" id="navegacao-eventos">
						<li class="active">
							<a data-toggle="tab" href="#evento1">
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item-navegacao1" >
									<p class="categoria_evento">							
										<?php the_field('categoria_evento_1'); ?>
									</p>
									<?php the_field('data_evento_1'); ?> - <?php the_field('local_evento_1'); ?> 
								</div>
							</a>
						</li>
							<li>
								<a data-toggle="tab" href="#evento2">
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item-navegacao2">
									<p class="categoria_evento">
										<?php the_field('categoria_evento_2'); ?>
									</p>
									<?php the_field('data_evento_2'); ?> - <?php the_field('local_evento_2'); ?> 
								</div>
								</a>
							</li>
						<li>
							<a data-toggle="tab" href="#evento3">
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item-navegacao3">
									<p class="categoria_evento">
										<?php the_field('categoria_evento_3'); ?>
									</p>
									<?php the_field('data_evento_3'); ?> - <?php the_field('local_evento_3'); ?> 
								</div>
								</a>
						</li>
						<li>
							<a data-toggle="tab" href="#evento4">
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item-navegacao4">
								<p class="categoria_evento">
									<?php the_field('categoria_evento_4'); ?>
								</p>
								<?php the_field('data_evento_4'); ?> - <?php the_field('local_evento_4'); ?> 
							</div>
							</a>
						</li>
					</ul>
				

				</div>
			</div>
			<hr>
			<div class="tab-content container">
			<!-- <div class="container"> -->
			<!-- 	<div class="row"> -->
					<div id="evento1" class="row tab-pane fade in active">
						<div class="conteudo-evento">
							<?php the_field('conteudo_evento_1'); ?>
						</div>
						<div class="container-tabela">
							<div class="horario-programa">
								<?php
								// check if the repeater field has rows of data
								if( have_rows('hora_programa_1') ): ?>

									<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 head">
										Horário
									</div>
									<div class="col-lg-10 col-md-10 col-sm-8 col-xs-8 head">
										Programa
									</div>

								<?php endif ?>
							</div>
							<div class="dados">
							<?php
							// check if the repeater field has rows of data
							if( have_rows('hora_programa_1') ):
							 	// loop through the rows of data
							    while ( have_rows('hora_programa_1') ) : the_row();
							      // display a sub field value
							  		echo '<div class="row">';
								  		echo '<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 celula">';
								        the_sub_field('horario_evento_1');
								      echo '</div>';
							      	echo '<div class="col-lg-10 col-md-10 col-sm-8 col-xs-8 celula">';
								       the_sub_field('programa_evento_1');
							       	echo '</div>';
						       	echo '</div>';
						       	echo '<hr>';  
							    endwhile;
							endif;
							?>
							</div>
						</div>
					</div>	
				<!-- </div> -->
				<!-- <div class="row"> -->
					<div id="evento2" class="row tab-pane fade">
						<div class="conteudo-evento">
							<?php the_field('conteudo_evento_2'); ?>
						</div>
						<div class="container-tabela">
							<div class="horario-programa">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 head">
									Horário
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-8 head">
									Programa
								</div>
							</div>
							<div class="dados">
							<?php
							// check if the repeater field has rows of data
							if( have_rows('hora_programa_1') ):
							 	// loop through the rows of data
							    while ( have_rows('hora_programa_1') ) : the_row();
							      // display a sub field value
							  		echo '<div class="row">';
								  		echo '<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 celula">';
								        the_sub_field('horario_evento_1');
								      echo '</div>';
							      	echo '<div class="col-lg-10 col-md-10 col-sm-8 col-xs-8 celula">';
								       the_sub_field('programa_evento_1');
							       	echo '</div>';
						       	echo '</div>';
						       	echo '<hr>';  
							    endwhile;
							endif;
							?>
							</div>
						</div>
					</div>	
				<!-- </div> -->
				<!-- <div class="row"> -->
					<div id="evento3" class="row tab-pane fade">
						<div class="conteudo-evento">
							<?php the_field('conteudo_evento_3'); ?>
						</div>
						<div class="container-tabela">
							<div class="horario-programa">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 head">
									Horário
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-8 head">
									Programa
								</div>
							</div>
							<div class="dados">
							<?php
							// check if the repeater field has rows of data
							if( have_rows('hora_programa_1') ):
							 	// loop through the rows of data
							    while ( have_rows('hora_programa_1') ) : the_row();
							      // display a sub field value
							  		echo '<div class="row">';
								  		echo '<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 celula">';
								        the_sub_field('horario_evento_1');
								      echo '</div>';
							      	echo '<div class="col-lg-10 col-md-10 col-sm-8 col-xs-8 celula">';
								       the_sub_field('programa_evento_1');
							       	echo '</div>';
						       	echo '</div>';
						       	echo '<hr>';  
							    endwhile;
							endif;
							?>
							</div>
						</div>
					</div>	
				<!-- </div> -->
				<!-- <div class="row"> -->
					<div id="evento4" class="row tab-pane fade">
						<div class="conteudo-evento">
							<?php the_field('conteudo_evento_4'); ?>
						</div>

						<div class="container-tabela">
							<div class="horario-programa">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 head">
									Horário
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-8 head">
									Programa
								</div>
							</div>
							<div class="dados">
							<?php
							// check if the repeater field has rows of data
							if( have_rows('hora_programa_1') ):
							 	// loop through the rows of data
							    while ( have_rows('hora_programa_1') ) : the_row();
							      // display a sub field value
							  		echo '<div class="row">';
								  		echo '<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 celula">';
								        the_sub_field('horario_evento_1');
								      echo '</div>';
							      	echo '<div class="col-lg-10 col-md-10 col-sm-8 col-xs-8 celula">';
								       the_sub_field('programa_evento_1');
							       	echo '</div>';
						       	echo '</div>';
						       	echo '<hr>';  
							    endwhile;
							endif;
							?>
							</div>
						</div>
					</div>	
				<!-- </div> -->
			<!-- </div> -->
			</div>
		</div>
	</div>
	<?php endwhile; endif; ?>
	<div class="botoes-reg">
			<div class="botoes">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="botao-participar">
								<a href="<?php bloginfo('url'); ?>/quero-visitar">
									<span>Quero participar</span>
								</a>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="botao-patrocinar"><a href="<?php bloginfo('url'); ?>/quero-patrocinar">
								<span>Quero patrocinar</span>
								</a>
							</div>
						</div>
					</div>
			</div>
	</div>		
				

</div>
<?php get_footer(); ?>