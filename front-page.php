<?php
/**
 * The template for displaying Home
 *
 * This is the template that display Home.
 *
 * @package WordPress
 * @subpackage Theme_Luapp
 * @author Descubra
 * @since Theme Luapp 1.0
 */
get_header();?>
<div id="geral">
<!-- VÍDEO -->
<div class="container-fluid">
	<div class="video-desktop">
		<div class="row">
			<video autoplay loop poster="<?php bloginfo('template_directory'); ?>/public/img/bg-banner-foto.jpg" class="bg_video">
				<?php $video = get_field('destaque_video'); 
				if( $video ): ?>
					<source src="<?php echo $video['url']; ?>" type="video/mp4">
				<?php endif; ?>
			</video>		
			<div class="row">
				<div class="container-fluid">
					<div class="logo-video">
						<img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/public/img/logo-branco.png" alt="Logo Innovative Experience em branco">
					</div>
					<div class="titulo-video">
						<h1>INNOVATIVE EXPERIENCE</h1>
					</div>
					<div class="divisor-video">
						<hr>
					</div>
					<div class="chamada-video">
						<p><?php the_field('destaque_slogan');?></p>
					</div>
					<div class="divisor-video">
						<hr>
					</div>
					<div class="chamada-video">
						<p><?php the_field('destaque_data');?></p>
					</div>
					<div class="chamada-video">
						<p><?php the_field('destaque_local');?></p>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="video-mobile">
			<video autoplay loop poster="<?php bloginfo('template_directory'); ?>/public/img/bg-banner-foto.jpg" class="bg_video">
				<source src="<?php bloginfo('template_directory'); ?>/public/video/" type="video/mp4">
			</video>		
			<div class="row">
				<div class="container-fluid">
					<div class="logo-video">
						<img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/public/img/logo-branco.png" alt="Logo Innovative Experience em branco">
					</div>
					<div class="titulo-video">
						<h1>INNOVATIVE EXPERIENCE</h1>

					</div>
					<div class="divisor-video">
						<hr>
					</div>
					<div class="chamada-video">
						<p><?php the_field('destaque_slogan');?></p>
					</div>
					<div class="divisor-video">
						<hr>
					</div>
					<div class="chamada-video">
						<p><?php the_field('destaque_data');?></p>
					</div>
					<div class="chamada-video">
						<p><?php the_field('destaque_local');?></p>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
<!-- VÍDEO -->

<!-- CHAMADA -->
<div class="sessao-chamada">
	<div class="container-fluid">
		<div class="row">
			<div class="chamada">
				<div class="titulo-chamada">
				  <h1><?php the_field('chamada_titulo');?></h1>
				 
				</div>
				<div class="divisor-chamada">
					<hr>
				</div>
				<div class="chamada-chamada">
					
						<?php the_field('chamada_texto');?>
					
				</div>
			</div>
		</div>
	</div>
</div>
<!-- CHAMADA -->
<!-- PRÊMIOS -->

<div class="sessao-premios">
	<div class="sombreamento-premios">
	     	
	</div>
	<div class="profundidade-premios">
		<div class="container">
			<div class="row">
				<div class="premios">
					<div class="chamada-premio">
						<h1><?php the_field('chamada_premios');?></h1>
					</div>
							<div class="col-md-6 com-sm-12">
								<?php $imagem_workshop = get_field('imagem_workshop'); ?>
								<div class="imagem-premio">
									<img src="<?php echo $imagem_workshop['url']; ?>" alt="<?php echo $imagem_workshop['alt']; ?>">
								</div>
								<div class="titulo-chamada">
									<h2><?php the_field('titulo_workshop'); ?></h2>
								</div>
								<div class="resumo-chamada">
									<div class="entry-content">
										<?php the_field('resumo_workshop'); ?> 
									</div>
								</div>
								<div class="botao">
								<a href="<?php bloginfo('url'); ?>/win/">
									<span class="botao-premio">
										Conheça mais
									</span>
								</a>
								</div>
								<?php wp_reset_postdata(); ?>
							</div>

								<div class="col-md-6 com-sm-12">

								<?php $imagem_pitch = get_field('imagem_pitch'); ?>
								<div class="imagem-premio">
									<img src="<?php echo $imagem_pitch['url']; ?>" alt="<?php echo $imagem_pitch['alt']; ?>">
								</div>
								<div class="titulo-chamada">
									<h2><?php the_field('titulo_pitch'); ?></h2>
								</div>
								<div class="resumo-chamada">
									<div class="entry-content">
										<?php the_field('resumo_pitch'); ?> 
									</div>
								</div>
								<div class="botao">
								<a href="<?php bloginfo('url'); ?>/premio/">
									<span class="botao-premio">
										Conheça mais
									</span>
								</a>
								</div>
								<?php wp_reset_postdata(); ?>
							</div>
				</div>
			</div>
		</div>
	</div>
</div>		
<!-- /PRÊMIOS -->
<!-- NANO -->
<?php 
	$args = array( 'post_type' => 'videos', 'posts_per_page' => 1 );
	$minha_query = new WP_Query( $args ); 
?>
<div class="sessao-nano">
	<div class="container-fluid">
		<div class="row">
			<div class="titulo-nano">
				<h1><?php the_field('nano_titulo');?></h1>
			</div>
			<div class="divisor-nano">
				<hr>
			</div>
			<div class="video-nano">
				<div class="video">
					<?php $nanovideo = the_field('nano_video');?>
					<?php wp_oembed_get($nanovideo); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /NANO -->
<!-- BLOG -->
<?php 
	$args = array( 'post_type' => 'post', 'posts_per_page' =>3 );
	$query_post = new WP_Query( $args ); 
?>
<div class="sessao-blog">
	<div class="container">
		<div class="row">
			<div class="titulo-blog">
			<?php 
			$obj = get_post_type_object( 'post' );
			
			 ?>
				<h1><?php echo $obj->labels->name; ?></h1>
			</div>
			<div class="divisor-blog">
				<hr>
			</div>
			<div class="posts">
			<?php if ( $query_post->have_posts() ) : ?>
					<?php while ( $query_post->have_posts() ) : $query_post->the_post(); ?>
				
				<div class="col-lg-4 col-md-6 col-sm-12">
					<div class="post">
					<ul>
						<li>
							<?php the_post_thumbnail(); ?>
							<div class="box">
								<div class="titulo-post">
									<?php the_title();?>
								</div>
								<div class="resumo-post">
									<?php echo wp_trim_words(get_the_excerpt(), 12);?>
								</div>
								<div class="botao-post">
									<a href="<?php the_permalink(); ?>">
										<span class="post-botao">Continue Lendo</span>
									</a>
								</div>
							</div>
						</li>
					</ul>
					</div>
				</div>
				<?php wp_reset_postdata(); ?>
				<?php endwhile;  ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<!-- /BLOG -->
	<!-- <div class="sessao-parceiros">
		<div class="container">
			<div class="row">
				<div class="titulo-parceiros">
					<h1>Parceiros e patrocinadores</h1>
				</div>
				<div class="divisor-parceiros">
					<hr>
				</div>
				<div class="patrocinio">
					<div class="container">
						Patrocínio <br>
						<hr>
						<div class="row">
						<?php 
					$args_patrocinio = array( 'post_type' => 'parceiros', 'parceiro_category' =>  'patrocinio', 'posts_per_page' =>20);
					$query_patrocinio = new WP_Query( $args_patrocinio );
						?>
					<?php if ( $query_patrocinio->have_posts() ) :?>
						<?php while ( $query_patrocinio->have_posts() ) : $query_patrocinio->the_post(); ?>
							<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
								<div class="parceiro-box">
									<ul>
										<li class="img-responsive">
											<?php the_post_thumbnail(); ?>
										</li>
									</ul>
								</div>
							</div>
						<?php endwhile;  ?>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>
						</div>
					</div>
				</div> 
				<div class="apoio">
					<div class="container">
					Apoio <br>
					<hr>
					<div class="row">
					<?php 
				$args_apoio = array( 'post_type' => 'parceiros', 'parceiro_category' =>  'apoio', 'posts_per_page' =>20);
				$query_apoio = new WP_Query( $args_apoio );
					?>
				<?php if ( $query_apoio->have_posts() ) :?>
					<?php while ( $query_apoio->have_posts() ) : $query_apoio->the_post(); ?>
						<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
							<div class="parceiro-box">
								<ul>
									<li class="img-responsive">
										<?php the_post_thumbnail(); ?>
									</li>
								</ul>
							</div>
						</div>
					
					<?php endwhile;  ?>
				<?php endif; ?>
				<?php wp_reset_postdata(); ?>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div> -->
</div>
<?php get_footer(); ?>
