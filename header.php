<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Theme_Luapp
 * @author Descubra
 * @since Theme Luapp 1.0
 */
?>
<!doctype html>
<html lang="pt-br">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width">
  <link rel="icon" type="image/png" href="<?php bloginfo( 'url' );?>/wp-content/themes/theme_unius/public/img/Innovativex.png">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/public/css/slick.css"/>
  <?php wp_head(); ?>
  <title>Innovative Experience - <?php the_title(); ?></title>
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-91624329-1', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
  <div id="fb-root"></div> 
  <script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));
  </script>
  
   <header id="masthead" class="site-header">
        <div class="navigation-background <?php if (is_home()) { echo "index-background"; } ?>">
            
            <nav class="navbar navbar-default">
              <div class="container-fluid">
                <div class="row"> 
                <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="container">
                    <div class="col-lg-12 col-md-12">
                      <div class="navbar-elements">
                        <div class="navbar-header">
                          <div class="img-header"> <a href="<?php bloginfo('url'); ?>">
                          <img src="<?php bloginfo('template_directory'); ?>/public/img/logo-header.png" alt="Logo Innovative Experience" class="img-responsive" ></a>
                          </div>
                          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" id="collapse-menu" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'main-navigation', 'menu' => 'Menu 1', 'menu_id' => 'menu-menu-1', 'fallback_cb' => 'wp_bootstrap_navwalker::fallback',  'depth' => 2, 'walker' => new wp_bootstrap_navwalker()) ); ?>

                        </div><!-- /.navbar-collapse -->
                      </div> <!-- navbar-elements -->
                    </div> <!-- col-lg-12 -->
                  </div> <!-- container -->
                   <div class="sociais"> 

                    <figure>
                      <a href="<?php echo get_option('facebook_unidade'); ?>" target="_blank">
                      <img class="default" src="<?php bloginfo('template_directory'); ?>/public/img/icone-facebook.png" alt="Ícone Facebook">
                      <img class="ativa" src="<?php bloginfo('template_directory'); ?>/public/img/icone-facebook-active.png" alt="Ícone Facebook Ativo">
                      </a>
                    </figure>

                    <figure>
                      <a href="<?php echo get_option('linkedin_unidade'); ?>" target="_blank">
                      <img class="default" src="<?php bloginfo('template_directory'); ?>/public/img/icone-linkedin.png" alt="Ícone Linkedin">
                      <img class="ativa" src="<?php bloginfo('template_directory'); ?>/public/img/icone-linkedin-active.png" alt="Ícone Linkedin Ativo">
                      </a>
                    </figure>
                   <!-- <figure>
                      <a href="<?php echo get_option('youtube_unidade'); ?>" target="_blank">
                      <img class="default" src="<?php bloginfo('template_directory'); ?>/public/img/icone-youtube.png" alt="Ícone Youtube">
                      <img class="ativa" src="<?php bloginfo('template_directory'); ?>/public/img/icone-youtube-active.png" alt="Ícone Youtube">
                      </a>
                    </figure> -->
                    <figure>
                      <a href="<?php echo get_option('twitter_unidade'); ?>" target="_blank">
                      <img class="default" src="<?php bloginfo('template_directory'); ?>/public/img/icone-twiter.png" alt="Ícone Twitter">
                      <img class="ativa" src="<?php bloginfo('template_directory'); ?>/public/img/icone-twiter-active.png" alt="Ícone Twitter Ativo">
                      </a>
                    </figure>
                   <!-- <figure>
                      <a href="<?php echo get_option('instagram_unidade'); ?>" target="_blank">    
                      <img class="default" src="<?php bloginfo('template_directory'); ?>/public/img/icone-instagram.png" alt="Ícone Instagram">
                      <img class="ativa" src="<?php bloginfo('template_directory'); ?>/public/img/icone-instagram-active.png" alt="Ícone Instagram Ativo">
                      </a>
                    </figure> -->
                  </div>
                </div><!-- row -->
              </div><!-- /.container-fluid -->
            </nav>     
        </div>
        
        
    </header><!-- #masthead -->