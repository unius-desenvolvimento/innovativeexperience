<?php
/**
 * The template for displaying Home
 *
 * This is the template that display Home.
 *
 * @package WordPress
 * @subpackage Theme_Luapp
 * @author Descubra
 * @since Theme Luapp 1.0
 */
get_header();?>
	<div class="patrocinar">
			<div class="container-fluid">
				<div class="banner">
					<div class="dados-destaque">
						<div class="row">
							<div class="dado">
								<?php the_field('patrocinar_data');?>
							</div>
						</div>
						<div class="row">
							<div class="dado">
								<?php the_field('patrocinar_local');?>
							</div>
						</div>
						<div class="row">
							<div class="dado"><?php the_field('patrocinar_horario');?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="conteudo">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<div class="row">
								<div class="titulo-quero-patrocinar">
									<h1><?php the_title();?></h1>
									<hr>
								</div>
							</div>
							<div class="row">
								<div class="container">
									<div class="conteudo">
										<?php the_content(); ?>
									</div>
								</div>
							</div>
						<?php endwhile; endif; ?>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="formulario-patrocinar">
					<div class="container">
						<?php echo do_shortcode('[contact-form-7 id="218" title="Quero Patrocinar"]'); ?>
					</div>
				</div>
			</div>
	</div>	
<?php get_footer(); ?>