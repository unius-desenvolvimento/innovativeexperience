<!-- ISSO É UM FORM DE CONTATO DE COMO ESTÁ NO PLUGIN. NADA CHAMA ESSA PÁGINA, ELA SÓ EXISTE DE BACKUP!!!! -->
<div class="formulario-patrocinar">
	<div class="checkbox-patrocinar">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>O que deseja patrocinar? <span>(Selecione uma ou mais opções)</span></p>
					</div>
					<div class="campo-check">
						[checkbox checkbox-266 "Innovative Experience" "Prêmio" "WIN Internacional" "WIN Regional"]
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="campos-gerais">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>Nome:</p>
					</div>
					<div class="campo">
						[text text-10 id:queroPatrocinarNome class:form-group]
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>Email:</p>
					</div>
					<div class="campo">
						[email email-740 id:queroPatrocinarEmail class:form-group]
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>Telefone:</p>
					</div>
					<div class="campo">
						[tel tel-508 id:queroPatrocinarTel class:form-group]
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>Empresa:</p>
					</div>
					<div class="campo">
						[text text-11 id:queroPatrocinarEmpresa class:form-group]
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>Site:</p>
					</div>
					<div class="campo">
						[text text-12 id:queroPatrocinarSite class:form-group]
					</div>
				</div>
			</div>
		</div>
		<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>Informar o feedback desejado em patrocinar o evento:</p>
					</div>
					<div class="campo">
						[textarea textarea-33 id:queroPatrocinarFeedback class:form-group]
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="botao-enviar">
		<div class="row">
			[submit id:queroPatrocinarEnviar "Enviar"]
		</div>
	</div>
</div>
<!-- ISSO É UM FORM DE CONTATO DE COMO ESTÁ NO PLUGIN. NADA CHAMA ESSA PÁGINA, ELA SÓ EXISTE DE BACKUP!!!! -->