<!-- ISSO É UM FORM DE CONTATO DE COMO ESTÁ NO PLUGIN. NADA CHAMA ESSA PÁGINA, ELA SÓ EXISTE DE BACKUP!!!! -->
<div class="formulario-visitar">
	<div class="campos-gerais">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>Nome:</p>
					</div>
					<div class="campo">
						[text text-10 id:queroVisitarNome class:form-group]
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>Email:</p>
					</div>
					<div class="campo">
						[email email-337 id:queroVisitarEmail class:form-group]
					</div>
				</div>
			</div>
		</div>
			<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>Telefone:</p>
					</div>
					<div class="campo">
						[tel tel-854 id:queroVisitarTel class:form-group]
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>Profissão:</p>
					</div>
					<div class="campo">
						[text text-856 id:querpVisitarProfissao class:form-group]
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>Empresa:</p>
					</div>
					<div class="campo">
						[text text-857 id:querpVisitarEmpresa class:form-group]
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>Site:</p>
					</div>
					<div class="campo">
						[text text-858 id:querpVisitarSite class:form-group]
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-12">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>CEP:</p>
					</div>
					<div class="campo">
						[text text-859 id:querpVisitarCep class:form-group]
					</div>
				</div>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-12">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>Endereço:</p>
					</div>
					<div class="campo">
						[text text-860 id:querpVisitarEndereco class:form-group]
					</div>
				</div>
			</div>
			<div class="col-lg-1 col-md-1 col-sm-12">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>N:</p>
					</div>
					<div class="campo">
						[number number-386 id:queroVisitarNumero class:form-group]
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>Complemento:</p>
					</div>
					<div class="campo">
						[text text-861 id:querpVisitarComplemento class:form-group]
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>Cidade:</p>
					</div>
					<div class="campo">
						[text text-862 id:querpVisitarCidade class:form-group]
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>Estado:</p>
					</div>
					<div class="campo">
						[text text-863 id:querpVisitarEstado class:form-group]
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>Tecnologias de seu interesse:</p>
					</div>
					<div class="campo">
						[select menu-105 id:queroVisitarTecnologia class:formgroup "Opção 1" "Opção 2" "Opção 3"]
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="form-group">
					<div class="label-quero-participar">
						<p>Como ficou sabendo do evento?</p>
					</div>
					<div class="campo">
						[select menu-106 id:queroVisitarComoFicouSabendo class:formgroup "Opção 1" "Opção 2" "Opção 3"]
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="botao-enviar">
		<div class="row">
			[submit id:queroVisitarEnviar "Enviar"]
		</div>
	</div>
</div>
<!-- ISSO É UM FORM DE CONTATO DE COMO ESTÁ NO PLUGIN. NADA CHAMA ESSA PÁGINA, ELA SÓ EXISTE DE BACKUP!!!! -->