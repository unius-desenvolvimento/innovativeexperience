const gulp = require('gulp')
      sass = require('gulp-sass'),
      concat = require('gulp-concat'),
      uglify = require('gulp-uglify'),
      pump = require('pump'),
      imagemin = require('gulp-imagemin'),
      watch = require('gulp-watch'),
      notify = require("gulp-notify"),
      autoprefixer = require('gulp-autoprefixer'),
      assets = "assets/src/";

var libs = {
  js : [
    assets+'js/lib/*.js',
    // assets + "vendor/bxslider/jquery.bxSlider.min.js",
    assets+"vendor/slick-carousel/slick/slick.js",
    assets+'js/*.js'
  ],
  css: [
    assets+'css/*.css',
    assets+'sass/*.scss'

  ]
};


gulp.task('default', ['sass','js','img']);

gulp.task('sass', function (cb) {
console.log(libs.css);
 pump([
    gulp.src(libs.css),
      autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
      }),
      concat('main.min.css'),
      sass({outputStyle: 'compressed'}).on('error', sass.logError),
      gulp.dest('public/css'),
      notify("É Hora do Show Porra!")
    ],
    cb
  );
});

gulp.task('js', function (cb) {
  console.log(libs.js);
  pump([
        gulp.src(libs.js),
        concat('main.min.js'),
        uglify(),
        gulp.dest('public/js'),
        notify("É Hora do Show Porra!")
    ],
    cb
  );
});

gulp.task('img', function (cb) {
  console.log(assets+'img/');
  pump([
        gulp.src(assets+'img/*'),
        imagemin(),
        gulp.dest('public/img'),
        notify("É Hora do Show Porra!")
    ],
    cb
  );
});

gulp.task('watch', function (cb) {

        gulp.watch([assets + 'css/*.css', assets+'sass/**/*.scss'], ['sass']);
        gulp.watch([assets+'js/lib/*.js', assets+'js/*.js'],['js']);
});