<?php
/**
 * The template for displaying Home
 *
 * This is the template that display Home.
 *
 * @package WordPress
 * @subpackage Theme_Luapp
 * @author Descubra
 * @since Theme Luapp 1.0
 */
get_header();?>

<?php get_search_query(); ?>

<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<?php 
	$args = array( 'post_type' => 'post', 's' => $s, 'posts_per_page' =>6, 'paged' => $paged );
	$query_post = new WP_Query( $args ); 
?>
<div class="listagem-posts">
	<div class="container-fluid">
		<div class="banner">
			<?php $obj = get_post_type_object( 'post' ); ?>
			<h1><?php echo $obj->labels->name; ?></h1>
		</div>
	</div>
	
	<div class="container">
		<div class="breadcrumb">
				<a href="<?php bloginfo('url'); ?>">HOME </a> <img src="<?php bloginfo('template_directory'); ?>/public/img/Innovativex.png" alt="favicon breadcrumb"><a href="<?php bloginfo('url'); ?>/blog"> BLOG</a> <img src="<?php bloginfo('template_directory'); ?>/public/img/Innovativex.png" alt="favicon breadcrumb"> <span style="color: #8d8c8c;">V</span><span style="text-transform: lowercase; color: #8d8c8c;">ocê pesquisou por </span> <img src="<?php bloginfo('template_directory'); ?>/public/img/Innovativex.png" alt="favicon breadcrumb"> <?php echo $s; ?>
		</div>

		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-12">
				<div class="posts">
				<div class="resposta">
					RESULTADOS DA BUSCA POR <span style="color:#000;">"<?php the_search_query(); ?>"</span>
				</div>
				<?php if ( $query_post->have_posts() ) : ?>
				<?php while ( $query_post->have_posts() ) : $query_post->the_post(); ?>
					<div class="row">
						<div class="post">
							<div class="col-lg-4 col-md-4 col-sm-12">
							<a href="<?php the_permalink(); ?>">
								<div class="img-post">
									<?php the_post_thumbnail(); ?>
								</div>
							</a>
							</div>
							<div class="col-lg-8 col-md-8 col-sm-12">
								<div class="textos">
									<div class="titulo-post">
									<a href="<?php the_permalink(); ?>">
										<h1><?php the_title();?></h1>
									</a>
									</div>
									<div class="resumo-post">
										<?php echo wp_trim_words( get_the_excerpt(), 25 );?>
										<a href="<?php the_permalink(); ?>">Continue Lendo</a>
									</div>
									<div class="divisor-compartilhar">
										Compartilhar <hr>
									</div>
									<div class="botoes-compartilhar">
										<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink();?>">
											<span class="facebook">Facebook</span>
										</a>
										<a href="https://twitter.com/home?status=<?php the_permalink();?>">
											<span class="twitter">Twitter</span>
										</a>
										<a href="https://plus.google.com/share?url=<?php the_permalink();?>">
											<span class="google">Google+</span>
										</a>
										<a href="https://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=&source=<?php the_permalink();?>">
											<span class="linkedin">Linkedin</span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="divisor-post">
								<hr>
					</div>
					<?php wp_reset_postdata(); ?>
					<?php endwhile;  ?>
					<?php endif; ?>
					<?php wp_reset_query(); ?>
					<?php wp_pagenavi( array( 'query' => $query_post ) ); ?>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="laterais">
					<div class="pesquisa">
						<?php get_search_form(); ?>
					</div>
					<div class="topicos-veja">
						<div class="titulo-topicos-veja">
							<h1>Veja mais</h1>
							<hr>
						</div>
						

						<?php	$args_veja = array( 'post_type' => 'post', 'posts_per_page' =>4 );
							$query_veja = new WP_Query( $args_veja ); ?>
							<?php if ( $query_veja->have_posts() ) : ?>
						<?php while ( $query_veja->have_posts() ) : $query_veja->the_post(); ?>
							<div class="row">	
								<div class="posts-veja">
									<div class="col-lg-4 col-md-4 col-sm-12">
										<div class="container-veja">
											<a href="<?php the_permalink(); ?>">
											<div class="img-veja">
												<?php the_post_thumbnail();?>
											</div>
											</a>
										</div>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-12">
									<a href="<?php the_permalink(); ?>">
										<div class="titulo-veja">
											<?php the_title();?>
										</div>
									</a>
									</div>
								</div>
							</div>	
							<?php endwhile;  ?>
							<?php endif; ?>
							<?php wp_reset_query(); ?>
						</div>
					<div class="categorias">
						<div class="lista">
							<ul>
								<?php wp_list_categories(); ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 

<?php get_footer(); ?>
