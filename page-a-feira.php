<?php
/**
 * The template for displaying Home
 *
 * This is the template that display Home.
 *
 * @package WordPress
 * @subpackage Theme_Luapp
 * @author Descubra
 * @since Theme Luapp 1.0
 */
get_header();?>
<div class="feira">
	<div class="container-fluid">
		<div class="banner">
			<div class="img-destaque">
				<?php the_post_thumbnail(); ?>
			</div>
			<div class="titulo-destaque">
				<div class="row">
					<div class="dado">
						<?php the_field('feira_titulo');?>
					</div>
				</div>
			</div>
			<div class="dados-destaque">
			<hr>
				<div class="row">
					<div class="dado">
						<?php the_field('feira_data');?>
					</div>
				</div>
				<div class="row">
					<div class="dado">
						<?php the_field('feira_local');?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="row">
					<div class="titulo-feira">
						<h1>INNOVATIVE EXPERIENCE</h1>
						<hr>
					</div>
				</div>
				<div class="row">
					<div class="conteudo">
						<?php the_content(); ?>
						<div class="relatorio">
							<a href="<?php the_field('feira_relatorio_download'); ?>"><p>RELATÓRIO DE RESULTADOS DA ÚLTIMA EDIÇÃO DA NANO TRADE SHOW E VENTURE CAPITAL EXPO.</p>
							<p>CLIQUE PARA DOWNLOAD</p>
							</a>
						</div>
					</div>
				</div>
				<div class="botoes">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-12">
							<div class="botao-expor">
								<a href="<?php bloginfo('url'); ?>/quero-expor">
									<span>Quero Expor</span>
									</a>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12">
							<div class="botao-patrocinar">
								<a href="<?php bloginfo('url'); ?>/quero-patrocinar">
									<span>Quero Patrocinar</span>
									</a>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12">
							<div class="botao-visitar">
							<a href="<?php bloginfo('url'); ?>/quero-visitar">
									<span>Quero Visitar</span>
							</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- container -->
	<?php endwhile; endif; ?>
	<div class="container-fluid">
		<div class="row">
			<div class="expoe">
				<div class="container">
					<div class="titulo-sessao-feira">
						<h1>QUEM EXPÕE?</h1>
						<hr>
					</div>
					<div class="conteudo-expoe">
						<?php the_field('feira_quem_expoe'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="visita">
				<div class="container">
					<div class="titulo-sessao-feira">
						<h1>QUEM VISITA?</h1>
						<hr>
					</div>
					<div class="conteudo-visita">
						<?php the_field('feira_quem_visita'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="botoes-inferiores">
				<div class="container">
					<div class="chamada-inferior">
							Participe do maior evento de tecnologias inovadoras.
					</div>
					<div class="botoes">
						
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div class="botao-expor">
								<a href="<?php bloginfo('url'); ?>/quero-expor">
									<span>Quero Expor</span>
								</a>	
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div class="botao-patrocinar">
								<a href="<?php bloginfo('url'); ?>/quero-patrocinar">
									<span>Quero Patrocinar</span>
								</a>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div class="botao-visitar">
								<a href="<?php bloginfo('url'); ?>/quero-visitar">
									<span>Quero Visitar</span>
									</a>
								</div>
							</div>
					
					</div>
					<div class="segunda-chamada-inferior">
						A expectativa é de atrair mais de 3 mil visitantes para movimentar um volume de negócios na ordem de R$ 15 Milhões.
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>