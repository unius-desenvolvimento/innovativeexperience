<form action="<?php echo home_url(); ?>" method="get" accept-charset="utf-8" id="searchform" role="search">

	<input placeholder="Procurar..." type="text" name="s" id="form-search" value="<?php the_search_query(); ?>" class="campo-pesquisa" />

	<button type="submit" class="lupa" > 
    	<i class="fa fa-search" aria-hidden="true"></i>
    </button>

</form>



