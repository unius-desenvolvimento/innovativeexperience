<?php
/**
 * The template for displaying Home
 *
 * This is the template that display Home.
 *
 * @package WordPress
 * @subpackage Theme_Luapp
 * @author Descubra
 * @since Theme Luapp 1.0
 */
get_header();?>
<div class="workshops">
	<div class="container-fluid">
		<div class="banner">
			<div class="img-destaque">
				<?php the_post_thumbnail(); ?>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div class="row">
					<div class="titulo-win">
						<h1><?php the_field('win_titulo');?></h1>
						<hr>
					</div>
				</div>
				<div class="row">
					<div class="conteudo">
						<?php the_content(); ?>
					</div>
				</div>
				<div class="botoes">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="botao-win-reg">
							<a href="<?php bloginfo('url'); ?>/win/win-regionais">
								<span>Workshops Regionais</span>
							</a>	
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="botao-win-int">
								<a href="<?php bloginfo('url'); ?>/win/win-internacional">
									<span>Workshop Internacional</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			<?php endwhile; endif; ?>
			</div>
		</div>
	</div><!-- container -->
</div>
<?php get_footer(); ?>