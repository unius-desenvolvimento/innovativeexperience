 $(document).ready(function(){ 

   // $( "#navegacao-eventos" ).tabs();
  

   $('html').on('DOMMouseScroll mousewheel', function (e) {
    if(e.originalEvent.detail > 0 || e.originalEvent.wheelDelta < 0) { //alternative options for wheelData: wheelDeltaX & wheelDeltaY
      //scroll down
    $("#collapse-menu").attr("aria-expanded", "false");
    $("#collapse-menu").addClass("collapsed");
    $("#bs-example-navbar-collapse-1").removeClass("in");
    $("#bs-example-navbar-collapse-1").attr("aria-expanded", "false");
 
    } 
    //prevent page fom scrolling
    //return false;
  });

 $("#queroExporSetor").change(function () {
  if ($(this).val() == "Outros") {

      $(".condicional").css("display" , "block");
  }              
});
    
$("#masthead ul li").each(function() { var el = $('#' + $(this).attr('id') + ' ul:eq(0)'); $(this).hover(function() {  var n_top = $(this).position(); var n_width = 190; el.css('right'); el.css('top', n_top); el.css('width', n_width); el.show(); }, function(){ el.hide(); }); });


  $('.vencedores-carrosel').slick({
     dots: false,
     prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
     nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,

    responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

});