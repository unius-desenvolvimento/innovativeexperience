<div class="campos-gerais">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="form-group">
				<div class="label-quero-participar">
					<p>Nome:</p>
				</div>
				<div class="campo">
					[text text-10 id:queroExporNome class:form-group]
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="form-group">
				<div class="label-quero-participar">
					<p>Empresa:</p>
				</div>
				<div class="campo">
					[text text-857 id:queroExporEmpresa class:form-group]
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="form-group">
				<div class="label-quero-participar">
					<p>Email:</p>
				</div>
				<div class="campo">
					[email email-337 id:queroExporEmail class:form-group]
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="form-group">
				<div class="label-quero-participar">
					<p>Telefone:</p>
				</div>
				<div class="campo">
					[tel tel-854 id:queroExporTel class:form-group]
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="form-group">
				<div class="label-quero-participar">
					<p>Site:</p>
				</div>
				<div class="campo">
					[text text-858 id:queroExporSite class:form-group]
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="form-group">
				<div class="label-quero-participar">
					<p>Intenção de área m²:</p>
				</div>
				<div class="campo">
					[text text-857 id:queroExporIntencao class:form-group]
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="form-group">
				<div class="label-quero-participar">
					<p>Setor que sua empresa faz parte:</p>
				</div>
				<div class="campo">
					[select menu-105 id:queroExporSetor class:formgroup "Biotecnologia" "Fotônica e Tecnologia Laser" "Materiais Avançados" "Microeletrônica e Semicondutores" "Nanotecnologia" "Robótica" "Tecnologia 3D" "Outros"]
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="form-group">
				<div class="label-quero-participar">
					<p>Como ficou sabendo do evento?</p>
				</div>
				<div class="campo">
					[select menu-105 id:queroExporSabendo class:formgroup "Indicação" "E-mail marketing" "Anúncio | Jornal | Site" "Google Awards" "Pesquisa na Internet" "Outros"]
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="form-group condicional">
				<div class="label-quero-participar">
					<p>Especifique:</p>
				</div>
				<div class="campo">
					[text text-446 id:queroExporEspecifique class:form-group]
				</div>
			</div>
		</div>
	</div>
</div>
<div class="botao-enviar">
	<div class="row">
		[submit id:queroExporEnviar "Enviar"]
	</div>
</div>