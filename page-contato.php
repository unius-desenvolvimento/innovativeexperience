<?php
/**
 * The template for displaying Home
 *
 * This is the template that display Home.
 *
 * @package WordPress
 * @subpackage Theme_Luapp
 * @author Descubra
 * @since Theme Luapp 1.0
 */
get_header();?>
<div class="contato">
    <div class="container label-container">        
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <p>Contato</p>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-hr">
                <hr class="contato-hr">
            </div>
        </div>
    </div>
    <div class="container form-container">        
        <?php echo do_shortcode('[contact-form-7 id="123" title="Formulário de contato 1" html_class="use-floating-validation-tip"]'); ?>            
    </div>
</div>
<div class="container contato-footer">
        <div class="row info">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <p><i class="fa fa-phone" aria-hidden="true"></i>11 3384-5218</p>

            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <p><i class="fa fa-envelope-o" aria-hidden="true"></i>contato@innovativex.com.br</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <ul>
                    <a href="<?php echo get_option('facebook_unidade'); ?>" target="_blank">
                         <li class="contato-footer-facebook"></li>
                    </a>
                    <a href="<?php echo get_option('linkedin_unidade'); ?>" target="_blank">
                        <li class="contato-footer-linkedin"></li>
                    </a>
                  <!--  <li class="contato-footer-youtube"></li> -->
                    <a href="<?php echo get_option('twitter_unidade'); ?>" target="_blank">
                        <li class="contato-footer-twiter"></li>
                    </a>
                <!--    <li class="contato-footer-instagram"></li> -->
                </ul>                
            </div>
        </div>
        <div class="row trademark">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <span>Desenvolvido por UNIUS</span>
            </div>
        </div>
</div>