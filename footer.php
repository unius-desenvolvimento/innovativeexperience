
<div style="background: #fff;height: 10px;width: 100%;"></div>
<div class="div-laranja-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="titulo-contato"><span>Contato</span></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="innovative-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="innovative-footer-contato">
                            <div class="dados-gerais">
                                <p>
                                    <a href="mailto:<?php echo get_option('email'); ?>">

                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    </a>
                                    <a href="mailto:<?php echo get_option('email'); ?>">
                                        <?php echo get_option('email'); ?>
                                    </a>
                                </p>
                                <p>
                                    <a href="tel:<?php echo str_replace('.','', str_replace(' ', '', get_option('tel1'))); ?>">
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                    </a>
                                    <a href="tel:<?php echo str_replace('.','', str_replace(' ', '', get_option('tel1'))); ?>">
                                        <?php echo get_option('tel1'); ?>
                                    </a>
                                </p>
                            </div>
                            <div class="dotpet-footer-sociais">
                                <ul>
                                    <li class="dotpet-footer-sociais-link">
                                        <a href="<?php echo get_option('facebook_unidade'); ?>" target="_blank">
                                            <img src="<?php bloginfo('template_directory'); ?>/public/img/icone-facebook-active.png" alt="Logo Facebook">
                                        </a>
                                    </li>
                                     <li class="dotpet-footer-sociais-link">
                                        <a href="<?php echo get_option('linkedin_unidade'); ?>" target="_blank">
                                            <img src="<?php bloginfo('template_directory'); ?>/public/img/icone-linkedin-active.png" alt="Logo Linkedin">
                                        </a>
                                    </li>
                                <!--     <li class="dotpet-footer-sociais-link">
                                        <a href="<?php echo get_option('youtube_unidade'); ?>" target="_blank">
                                            <img src="<?php bloginfo('template_directory'); ?>/public/img/icone-youtube-active.png" alt="Logo Youtube">
                                        </a>
                                    </li> -->

                                    <li class="dotpet-footer-sociais-link">
                                        <a href="<?php echo get_option('twitter_unidade'); ?>" target="_blank">
                                            <img src="<?php bloginfo('template_directory'); ?>/public/img/icone-twiter-active.png" alt="Logo Twitter">
                                        </a>
                                    </li>
                                   <!--  <li class="dotpet-footer-sociais-link">
                                        <a href="<?php echo get_option('instagram_unidade'); ?>" target="_blank">
                                            <img src="<?php bloginfo('template_directory'); ?>/public/img/icone-instagram-active.png" alt="Logo Instagram">
                                        </a>
                                    </li>   -->                               
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 hidden-sm hidden-xs">

                        <?php echo do_shortcode('[contact-form-7 id="124" title="Footer" html_class="use-floating-validation-tip"]'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            Desenvolvido por <a target="_blank" href="http://www.agenciaunius.com.br">Unius</a>
        </div>
        <?php //wp_footer(); ?>

</body>
</html>
