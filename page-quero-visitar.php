<?php
/**
 * The template for displaying Home
 *
 * This is the template that display Home.
 *
 * @package WordPress
 * @subpackage Theme_Luapp
 * @author Descubra
 * @since Theme Luapp 1.0
 */
get_header();?>
	<div class="visitar">
			<div class="container-fluid">
				<div class="banner">
					<div class="dados-destaque">
						<div class="row">
							<div class="dado">
								<?php the_field('visitar_data');?>
							</div>
						</div>
						<div class="row">
							<div class="dado">
								<?php the_field('visitar_local');?>
							</div>
						</div>
						<div class="row">
							<div class="dado"><?php the_field('visitar_horario');?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="conteudo">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<div class="row">
								<div class="titulo-quero-visitar">
									<h1><?php the_title();?></h1>
									<hr>
								</div>
							</div>
							<div class="row">
								<div class="container">
									<div class="conteudo">
										<?php the_content(); ?>
									</div>
								</div>
							</div>
						<?php endwhile; endif; ?>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="formulario-patrocinar">
					<div class="container">
						<?php echo do_shortcode('[contact-form-7 id="225" title="Quero Visitar"]'); ?>
					</div>
				</div>
			</div>
	</div>	
<?php get_footer(); ?>