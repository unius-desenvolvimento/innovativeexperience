<?php
/**
 * The template for displaying Home
 *
 * This is the template that display Home.
 *
 * @package WordPress
 * @subpackage Theme_Luapp
 * @author Descubra
 * @since Theme Luapp 1.0
 */
get_header();?>
	<div class="inscreva">
			<div class="container-fluid">
				<div class="banner">
					<div class="img-destaque">
						<?php the_post_thumbnail(); ?>
					</div>
					<div class="dados-destaque">
					<hr>
						<div class="row">
							<div class="dado">
								<?php the_field('inscreva_data');?>
							</div>
						</div>
						<div class="row">
							<div class="dado">
								<?php the_field('inscreva_local');?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="formulario-inscreva">
					<div class="container">
					<h1>INSCRIÇÃO</h1>
					<hr>
						<?php echo do_shortcode('[contact-form-7 id="309" title="Inscreva-se"]'); ?>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="tabela-inscreva">
					<div class="container">
						<div class="row">
						<div class="titulo-tabela">
							<h1>PESO DE ANÁLISE PARA AS QUESTÕES</h1>
							<p>O peso de análise dos critérios varia de 0 a 5, conforme tabela abaixo:</p>
						</div>
						<div class="tabela">
							<table>
								<tr>
									<td>CRITÉRIOS | CATEGORIAS</td>
									<td>PESQUISA</td>
									<td>PRODUTO PROCESSO</td>
									<td>FORNECEDOR</td>
									<td>PROFISSIONAL</td>
								</tr>
								<tr>
									<td>Custo x benefício</td>
									<td>1</td>
									<td>5</td>
									<td>5</td>
									<td>1</td>
								</tr>
								<tr>
									<td>Resultados alcançados com aplicação na indústria</td>
									<td>0</td>
									<td>0</td>
									<td>5</td>
									<td>1</td>
								</tr>
								<tr>
									<td>Análise comparativa com produto/tecnologia similar</td>
									<td>3</td>
									<td>5</td>
									<td>5</td>
									<td>2</td>
								</tr>
								<tr>
									<td>Possibilidade de aplicação em outros setores</td>
									<td>5</td>
									<td>0</td>
									<td>4</td>
									<td>3</td>
								</tr>
								<tr>
									<td>Viabilidade e impacto econômico/social/ambiental</td>
									<td>5</td>
									<td>5</td>
									<td>5</td>
									<td>5</td>
								</tr>
								<tr>
									<td>Originalidade</td>
									<td>4</td>
									<td>3</td>
									<td>3</td>
									<td>5</td>
								</tr>
								<tr>
									<td>Parceiros chaves para desenvolvimento da inovação</td>
									<td>3</td>
									<td>4</td>
									<td>4</td>
									<td>1</td>
								</tr>
								<tr>
									<td>Caracterização e extensão do mercado alvo</td>
									<td>2</td>
									<td>3</td>
									<td>3</td>
									<td>1</td>
								</tr>
							</table>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>	
<?php get_footer(); ?>