<?php
/**
 * The template for displaying Home
 *
 * This is the template that display Home.
 *
 * @package WordPress
 * @subpackage Theme_Luapp
 * @author Descubra
 * @since Theme Luapp 1.0
 */
get_header();?>

<div class="container-fluid">
	<div class="banner">
		<?php $obj = get_post_type_object( 'post' ); ?>
		<h1><?php echo $obj->labels->name; ?></h1>
	</div>
</div>
<div class="conteudo">
<div class="container">
	<div class="breadcrumb">
				<a href="<?php bloginfo('url'); ?>"> HOME </a><img src="<?php bloginfo('template_directory'); ?>/public/img/Innovativex.png" alt="favicon breadcrumb"><a href="<?php bloginfo('url'); ?>/blog"> BLOG </a><img src="<?php bloginfo('template_directory'); ?>/public/img/Innovativex.png" alt="favicon breadcrumb"> <?php wp_title($sep = ''); ?>
		</div>
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12">
			<?php while ( have_posts() ) : the_post(); ?>
			<div class="single">
				<div class="titulo-single">
						<h1><?php the_title();?></h1>
				</div>
				<div class="img-single">
					<?php the_post_thumbnail(); ?>
				</div>
				<div class="conteudo-single">
					<?php the_content(); ?>
				</div>
				<div class="divisor-compartilhar">
					Compartilhar <hr>
				</div>
				<div class="botoes-compartilhar">
					<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink();?>">
						<span class="facebook">Facebook</span>
					</a>
					<a href="https://twitter.com/home?status=<?php the_permalink();?>">
						<span class="twitter">Twitter</span>
					</a>
					<a href="https://plus.google.com/share?url=<?php the_permalink();?>">
						<span class="google">Google+</span>
					</a>
					<a href="https://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=&source=<?php the_permalink();?>">
						<span class="linkedin">Linkedin</span>
					</a>
				</div>
			</div>
			<div class="comentarios">
				<div class="titulo-comentarios">
						<h1>Deixe um comentário</h1>
						<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="700" data-numposts="7"></div>
				</div>
			</div>
			<div class="pode-gostar">
				<div class="titulo-pode-gostar">
						<h1>Você também pode gostar destes</h1>
						<hr>
				</div>
				<div class="posts-pode-gostar">
					<div class="container-pode-gostar">
				<?php $catId =  get_the_category(); ?>

				<?php $categoria =  strval($catId[0]->cat_ID);?>

				<?php	$args_gostar = array( 'post_type' => 'post', 'cat' =>  $categoria, 'posts_per_page' =>4, 'orderby' => 'rand');
					$query_gostar = new WP_Query( $args_gostar );?>
				<?php if ( $query_gostar->have_posts() ) :?>
					<?php while ( $query_gostar->have_posts() ) : $query_gostar->the_post(); ?>
				
					<div class="col-lg-3 col-md-3 col-sm-12">
						<div class="img-pode-gostar">
							<a href="<?php the_permalink(); ?>">
								<?php the_post_thumbnail();?>
							</a>
						</div>
						<div class="titulo-pode-gostar">
							<a href="<?php the_permalink(); ?>">
								<?php the_title();?>
							</a>
						</div>
					</div>
				<?php endwhile;  ?>
				<?php endif; ?>
				<?php wp_reset_query(); ?>
				</div>
				</div>		
			</div>
			<?php endwhile; ?>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12">
		<div class="laterais">
					<div class="pesquisa">
						<?php get_search_form(); ?>
					</div>
					<div class="topicos-relacionados">
						<div class="titulo-topicos-relacionados">
							<h1>Tópicos relacionados</h1>
							<hr>
						</div>
						<?php $catId =  get_the_category(); ?>
						<?php $categoria =  strval($catId[0]->cat_ID);?>

						<?php	$args_relacionado = array( 'post_type' => 'post', 'cat' =>  $categoria, 'posts_per_page' =>4, 'orderby' => 'asc');
								$query_relacionado = new WP_Query( $args_relacionado );?>
							<?php if ( $query_relacionado->have_posts() ) :?>
								<?php while ( $query_relacionado->have_posts() ) : $query_relacionado->the_post(); ?>
							<div class="row">	
								<div class="posts-relacionados">
									<div class="col-lg-4 col-md-4 col-sm-12">
										<div class="container-relacionados">
											<div class="img-relacionados">
											<a href="<?php the_permalink(); ?>">
												<?php the_post_thumbnail();?>
											</a>
											</div>
										</div>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-12">
										<div class="titulo-relacionados">
										<a href="<?php the_permalink(); ?>">
											<?php the_title();?>
										</a>
										</div>
									</div>
								</div>
							</div>	
							<?php endwhile;  ?>
							<?php endif; ?>
							<?php wp_reset_query(); ?>
						</div>
						
					<div class="categorias">
						<div class="lista">
							<ul>
								<?php wp_list_categories(); ?>
							</ul>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
</div>
<?php get_footer(); ?>
