<div class="row">
   <div class="col-lg-2 col-md-2 col-sm-2 label"><label> Nome: </label></div>
   <div class="col-lg-9 col-md-9 col-sm-9 field">[text* your-name]</div>
</div>
<div class="row">
   <div class="col-lg-2 col-md-2 col-sm-2 label"><label>E-mail:</label></div>
   <div class="col-lg-9 col-md-9 col-sm-9 field">[email* your-email]</div>
</div>
<div class="row">
   <div class="col-lg-2 col-md-2 col-sm-2 label"><label>Assunto:</label></div>
   <div class="col-lg-9 col-md-9 col-sm-9 field">[text your-subject]</div>
</div>
<div class="row">
   <div class="col-lg-2 col-md-2 col-sm-2 label"><label>Mensagem:</label></div>
   <div class="col-lg-9 col-md-9 col-sm-9 field">[textarea your-message]</div>
</div>
<div class="row">
   <div class="col-lg-offset-2 col-mg-offset-2 col-sm-offset-2 col-lg-9 col-md-9 col-sm-9 field">[submit "Enviar"]</div>
</div>