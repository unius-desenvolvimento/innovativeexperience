<?php
/**
 * @package WordPress
 * @subpackage Theme_Luapp
 * @since Theme Lunnar Application 1.0
 */

/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Theme Lunnar Application 1.0
 */

define( 'LUAPP_PATH', get_template_directory() . '/' );

define( 'LUAPP_INC', LUAPP_PATH . 'inc/' );

// Include the customizer functionality
require_once LUAPP_INC . 'customizer.php';

//Include the enqueues
require_once LUAPP_INC . 'enqueues.php';

//Include the theme_functions
require_once LUAPP_INC . 'theme_functions.php';

// Include the cusom appearance menu, to footer, header...
require_once LUAPP_INC . 'admin_custom.php';

// Include Bootstrap Nav Walker
require_once LUAPP_INC . 'wp_bootstrap_navwalker.php';


/*
* Creating a function to create our CPT
*/

function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Blog';
    $submenu['edit.php'][5][0] = 'Blog';
    $submenu['edit.php'][10][0] = 'Add Post';
    $submenu['edit.php'][16][0] = 'Blog Tags';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Blog';
    $labels->singular_name = 'Blog';
    $labels->add_new = 'Add Post';
    $labels->add_new_item = 'Add Post';
    $labels->edit_item = 'Edit Post';
    $labels->new_item = 'Post';
    $labels->view_item = 'View Post';
    $labels->search_items = 'Search Posts';
    $labels->not_found = 'No Post found';
    $labels->not_found_in_trash = 'No Posts found in Trash';
    $labels->all_items = 'All Posts';
    $labels->menu_name = 'Blog';
    $labels->name_admin_bar = 'Blog';
   
}
 
add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );





/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/


add_action( 'init', 'create_taxonomy_parceiro_category' );

function create_taxonomy_parceiro_category() {
    register_taxonomy( 'parceiro_category', array( 'parceiros' ), array(
        'hierarchical' => true,
        'label' => __( 'Categoria de Parceiro' ),
        'show_ui' => true,
        'show_in_tag_cloud' => true,
        'query_var' => true,
        'rewrite' => true,
        )
    );
}





function custom_post_type_parceiros() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Parceiros', 'Post Type General Name', 'theme_unius' ),
		'singular_name'       => _x( 'Parceiro', 'Post Type Singular Name', 'theme_unius' ),
		'menu_name'           => __( 'Parceiros', 'theme_unius' ),
		'all_items'           => __( 'Todos parceiros', 'theme_unius' ),
		'view_item'           => __( 'Ver parceiro', 'theme_unius' ),
		'add_new_item'        => __( 'Adicionar novo Parceiro', 'theme_unius' ),
		'add_new'             => __( 'Adicionar novo', 'theme_unius' ),
		'edit_item'           => __( 'Editar parceiros', 'theme_unius' ),
		'update_item'         => __( 'Atualizar parceiros', 'theme_unius' ),
		'search_items'        => __( 'Procurar parceiros', 'theme_unius' ),
		'not_found'           => __( 'Não encontrado', 'theme_unius' ),
		'not_found_in_trash'  => __( 'Não encontrado na lixeira', 'theme_unius' ),
	);
	
// Set other options for Custom Post Type
	
	$args = array(
		'label'               => __( 'parceiros', 'theme_unius' ),
		'description'         => __( 'Parceiros', 'theme_unius' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title','thumbnail'),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',

		

	);
	
	// Registering your Custom Post Type
	register_post_type( 'parceiros', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/

add_action( 'init', 'custom_post_type_parceiros', 0 );

/*PAGINAÇÃO*/

