<div class="campos-gerais">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="form-group">
				<div class="label-quero-participar">
					<p>Nome:</p>
				</div>
				<div class="campo">
					[text text-10 id:inscrevaNome class:form-group]
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="form-group">
				<div class="label-quero-participar">
					<p>Empresa:</p>
				</div>
				<div class="campo">
					[text text-857 id:inscrevaEmpresa class:form-group]
				</div>
			</div>
		</div>
	</div>
		<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="form-group">
				<div class="label-quero-participar">
					<p>Telefone:</p>
				</div>
				<div class="campo">
					[tel tel-854 id:inscrevaTel class:form-group]
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="form-group">
				<div class="label-quero-participar">
					<p>Email:</p>
				</div>
				<div class="campo">
					[email email-337 id:inscrevaEmail class:form-group]
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-12">
			<div class="form-group">
				<div class="label-quero-participar">
					<p>CEP:</p>
				</div>
				<div class="campo">
					[text text-859 id:inscrevaCep class:form-group]
				</div>
			</div>
		</div>
		<div class="col-lg-8 col-md-8 col-sm-12">
			<div class="form-group">
				<div class="label-quero-participar">
					<p>Endereço:</p>
				</div>
				<div class="campo">
					[text text-860 id:inscrevaEndereco class:form-group]
				</div>
			</div>
		</div>
		<div class="col-lg-1 col-md-1 col-sm-12">
			<div class="form-group">
				<div class="label-quero-participar">
					<p>N:</p>
				</div>
				<div class="campo">
					[text text-856 id:inscrevaNum class:form-group]
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-12">
			<div class="form-group">
				<div class="label-quero-participar">
					<p>Complemento:</p>
				</div>
				<div class="campo">
					[text text-861 id:inscrevaComplemento class:form-group]
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12">
			<div class="form-group">
				<div class="label-quero-participar">
					<p>Cidade:</p>
				</div>
				<div class="campo">
					[text text-862 id:inscrevaCidade class:form-group]
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12">
			<div class="form-group">
				<div class="label-quero-participar">
					<p>Estado:</p>
				</div>
				<div class="campo">
					[text text-863 id:inscrevaEstado class:form-group]
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="checks">
			<h1>Categoria</h1>
		</div>
		<div class="label-inscreva">
			<p>Indicar uma categoria em qual deseja se inscrever</p>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			[checkbox checkbox-16 id:melhorInovacao-produtoProcesso class:form-group "Melhor Pesquisa em Inovação com Tecnologia Chave (KET's)" "Produto ou Processo Mais Inovador (com KET's)"]
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			[checkbox checkbox-16 id:melhorFornecedor-profissional class:form-group "Melhor Fornecedor de Inovação de Tecnologia Chave (KET's)" "Profissional Destaque"]
		</div>
	</div>
	
	<div class="espaco">
		<p>Informar o nome da Pesquisa, Produto, Processo, Tecnologia ou Nome do Profissional</p>
		[text text-911 id:nomePesquisaInscreva class:form-group]
	</div>
	<div class="espaco">
		<p>Preencha abaixo os campos a serem analisados na premiação:</p>
	</div>
	<div class="espaco">
		<p>Descreva em 5 linhas o custo x benefício do projeto apresentado:</p>
		[textarea textarea-278 id:custoxBeneficioInscreva class:form-group]
	</div>
	<div class="espaco">
		<p>Descreva em 15 linhas os resultados alcançados com aplicação na indústra:</p>
		[textarea textarea-279 id:resultadosInscreva class:form-group]
	</div>
	<div class="espaco">
		<p>Faça uma análise comparativa com produto similar destacando os principais pontos de inovação (limitado 5 tópicos):</p>
		[textarea textarea-280 id:pontosInovacaoInscreva class:form-group]
	</div>
	<div class="espaco">
		<p>Informa a possibilidade de aplicação em outros setores - indicar quais setores e especificar o benefício, sendo 1 por linha, conforme exemplo (<span style="color:#828181"><i>Alimentos – Antimicrobiano</i></span>):</p>
		[textarea textarea-281 id:possibilidadeInscreva class:form-group]
	</div>
	<div class="espaco">
		<p>Descrever em 15 linhas a viabilidade e impacto econômico/social/ambiental:</p>
		[textarea textarea-282 id:viabilidadeInscreva class:form-group]
	</div>
	<div class="espaco">
		<p>Originalidade (a análise será pelo conjunto da obra):</p>
		[textarea textarea-283 id:originalidadeInscreva class:form-group]
	</div>
	<div class="espaco">
		<p>Indique os parceiros chaves para desenvolvimento da inovação (mencionar apenas nomes dos parceiros):</p>
		[textarea textarea-284 id:parceirosInscreva class:form-group]
	</div>
	<div class="espaco">
		<p>Caracterização e extensão do mercado alvo:</p>
		[textarea textarea-285 id:caracterInscreva class:form-group]
	</div>
	<div class="espaco">
		<p>Indique os parceiros chaves para desenvolvimento da inovação (mencionar apenas nomes dos parceiros):</p>
		[textarea textarea-285 id:viabilidadeInscreva class:form-group]
	</div>

	

</div>
<div class="botao-enviar">
	<div class="row">
		[submit id:inscrevaEnviar "Enviar"]
	</div>
</div>