<?php
/**
 * The template for displaying Home
 *
 * This is the template that display Home.
 *
 * @package WordPress
 * @subpackage Theme_Luapp
 * @author Descubra
 * @since Theme Luapp 1.0
 */
get_header();?>
<div class="premios">
	<div class="container-fluid">
		<div class="banner">
			<div class="img-destaque">
				<?php the_post_thumbnail(); ?>
			</div>
			
			<div class="dados-destaque">
			<hr>
				<div class="row">
					<div class="dado">
						<?php the_field('premio_data_hora');?>
					</div>
				</div>
				<div class="row">
					<div class="dado">
						<?php the_field('premio_local');?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<div class="sessao-premio-inovacao">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<div class="row">
							<div class="titulo-premio">
								<h1>Prêmio de inovação com tecnologias chaves</h1>
								<hr>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="conteudo">
						<?php the_content(); ?>
						<div class="botoes">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12 coluna-botao">
									<div class="botao-regulamento">
										<a href="<?php the_field('premio_regulamento'); ?>">
											<span>Regulamento</span>
										</a>
									</div>
								</div> 	
								<div class="col-lg-6 col-md-6 col-sm-12 coluna-botao">
									<div class="botao-inscrever">
									<a href="<?php bloginfo('url'); ?>/inscreva">
											<span>Inscreva-se</span>
									</a>
									</div>
								</div>
							</div>
						</div><!-- botoes -->
					</div>
				</div>
			</div>
		</div>
	<?php endwhile; endif; ?>	
	</div>
	<div class="sessao-categoria-premio">
		<div class="container-fluid">
			<div class="row">
				<div class="categoria">
					<div class="container">
						<div class="titulo-sessao-premio">
							<h1>CATEGORIAS DO PRÊMIO</h1>
							<hr>
						</div>
						<div class="conteudo-categoria">
							<?php the_field('premio_categoria'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="sessao-vencedores-premio">
		<div class="container-fluid">
			<div class="row">
				<div class="vencedores">
					<div class="container">
						<div class="titulo-sessao-premio-vencedores">
							<h1>CONHEÇA OS VENCEDORES DO PRÊMIO DE 2016</h1>
							<hr>
						</div>
						<div class="vencedores-carrosel">
							<?php the_field('premio_vencedores_carrossel'); ?>
						</div>
						<div class="conteudo-vencedores">
							<?php the_field('premio_vencedores_conteudo'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="sessao-botoes-inferiores">
		<div class="container-fluid">
			<div class="row">
				<div class="chamada">
					Não perca a oportunidade de concorrer ao único Prêmio De Inovação Com Tecnologias Chaves do Brasil 
				</div>

					<div class="botoes">
						<div class="col-lg-4 col-md-4 col-sm-12">
							<div class="botao-regulamento">
							<a href="<?php the_field('premio_regulamento'); ?>">
								<span>Regulamento</span>
							</a>	
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12">
							<div class="botao-inscreva">
							<a href="<?php bloginfo('url'); ?>/inscreva">
								<span>Inscreva-se</span>
							</a>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12">
							<div class="botao-patrocinar">
							<a href="<?php bloginfo('url'); ?>/quero-patrocinar">
								<span>Quero Patrocinar</span>
								</a>
							</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>